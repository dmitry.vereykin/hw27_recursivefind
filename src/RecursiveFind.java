import java.util.Scanner;

public class RecursiveFind
{//Start of class
   static int start = 0;
   static int end;

   public static void main(String[] args)
   {//Start of main
      Scanner in = new Scanner(System.in);
      System.out.print("Enter the string  being searched: ");
      String string = in.nextLine();
      System.out.print("Enter the word we're looking for: ");
      String word = in.nextLine();

      end = word.length();
      boolean b = find(string, word);
   		
      if (b) 
      {
         System.out.println("The word \"" + word + "\" is found in that string.");
      }
      else
      {
         System.out.println("The word \"" + word + "\" is not found in that string.");
      }
   }//End of main

   public static boolean find(String str, String word) {//Start of the find method.

      if (str.length() == 0 || str.length() < word.length() || word.length() == 0) {
         return false;
      } else if (start > str.length() - word.length()) {
         return false;
      } else if (str.equals(word)) {
         return true;
      } else if ((str.substring(start, end)).equalsIgnoreCase(word)) {
         return true;
      } else {
         start += 1;
         end += 1;
      }

      return find(str, word);
   }//End of the find method.
}//End of class